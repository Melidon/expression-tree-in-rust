#![feature(unboxed_closures)]
#![feature(fn_traits)]

use std::str::FromStr;

// https://en.wikipedia.org/wiki/Binary_expression_tree#Algebraic_expressions
enum Expression {
    Add(Box<Expression>, Box<Expression>),
    Sub(Box<Expression>, Box<Expression>),
    Mul(Box<Expression>, Box<Expression>),
    Div(Box<Expression>, Box<Expression>),
    Abs(Box<Expression>),
    Signum(Box<Expression>),
    Powf(Box<Expression>, Box<Expression>),
    Sqrt(Box<Expression>),
    Exp(Box<Expression>),
    Ln(Box<Expression>),
    Sin(Box<Expression>),
    Cos(Box<Expression>),
    Tan(Box<Expression>),
    Asin(Box<Expression>),
    Acos(Box<Expression>),
    Atan(Box<Expression>),
    Atan2(Box<Expression>, Box<Expression>),
    Sinh(Box<Expression>),
    Cosh(Box<Expression>),
    Tanh(Box<Expression>),
    Asinh(Box<Expression>),
    Acosh(Box<Expression>),
    Atanh(Box<Expression>),
    Var,
    Num(f64),
}

impl FnOnce<(f64,)> for Expression {
    type Output = f64;

    extern "rust-call" fn call_once(mut self, args: (f64,)) -> Self::Output {
        self.call_mut(args)
    }
}

impl FnMut<(f64,)> for Expression {
    extern "rust-call" fn call_mut(&mut self, args: (f64,)) -> Self::Output {
        self.call(args)
    }
}

impl Fn<(f64,)> for Expression {
    extern "rust-call" fn call(&self, args: (f64,)) -> Self::Output {
        let x = args.0;
        match self {
            Expression::Add(a, b) => a(x) + b(x),
            Expression::Sub(a, b) => a(x) - b(x),
            Expression::Mul(a, b) => a(x) * b(x),
            Expression::Div(a, b) => a(x) / b(x),
            Expression::Abs(a) => a(x).abs(),
            Expression::Signum(a) => a(x).signum(),
            Expression::Powf(a, b) => a(x).powf(b(x)),
            Expression::Sqrt(a) => a(x).sqrt(),
            Expression::Exp(a) => a(x).exp(),
            Expression::Ln(a) => a(x).ln(),
            Expression::Sin(a) => a(x).sin(),
            Expression::Cos(a) => a(x).cos(),
            Expression::Tan(a) => a(x).tan(),
            Expression::Asin(a) => a(x).asin(),
            Expression::Acos(a) => a(x).acos(),
            Expression::Atan(a) => a(x).atan(),
            Expression::Atan2(a, b) => a(x).atan2(b(x)),
            Expression::Sinh(a) => a(x).sinh(),
            Expression::Cosh(a) => a(x).cosh(),
            Expression::Tanh(a) => a(x).tanh(),
            Expression::Asinh(a) => a(x).asinh(),
            Expression::Acosh(a) => a(x).acosh(),
            Expression::Atanh(a) => a(x).atanh(),
            Expression::Var => x,
            Expression::Num(n) => *n,
        }
    }
}

// https://en.wikipedia.org/wiki/Reverse_Polish_notation
impl FromStr for Expression {
    type Err = ();

    fn from_str(expression: &str) -> Result<Self, Self::Err> {
        let mut stack = Vec::new();
        for token in expression.split(' ') {
            match token {
                "+" => {
                    let b = stack.pop().ok_or(())?;
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Add(Box::new(a), Box::new(b)));
                }
                "-" => {
                    let b = stack.pop().ok_or(())?;
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Sub(Box::new(a), Box::new(b)));
                }
                "*" => {
                    let b = stack.pop().ok_or(())?;
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Mul(Box::new(a), Box::new(b)));
                }
                "/" => {
                    let b = stack.pop().ok_or(())?;
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Div(Box::new(a), Box::new(b)));
                }
                "abs" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Abs(Box::new(a)));
                }
                "signum" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Signum(Box::new(a)));
                }
                "powf" => {
                    let b = stack.pop().ok_or(())?;
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Powf(Box::new(a), Box::new(b)));
                }
                "sqrt" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Sqrt(Box::new(a)));
                }
                "exp" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Exp(Box::new(a)));
                }
                "ln" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Ln(Box::new(a)));
                }
                "sin" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Sin(Box::new(a)));
                }
                "cos" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Cos(Box::new(a)));
                }
                "tan" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Tan(Box::new(a)));
                }
                "asin" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Asin(Box::new(a)));
                }
                "acos" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Acos(Box::new(a)));
                }
                "atan" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Atan(Box::new(a)));
                }
                "atan2" => {
                    let b = stack.pop().ok_or(())?;
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Atan2(Box::new(a), Box::new(b)));
                }
                "sinh" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Sinh(Box::new(a)));
                }
                "cosh" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Cosh(Box::new(a)));
                }
                "tanh" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Tanh(Box::new(a)));
                }
                "asinh" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Asinh(Box::new(a)));
                }
                "acosh" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Acosh(Box::new(a)));
                }
                "atanh" => {
                    let a = stack.pop().ok_or(())?;
                    stack.push(Expression::Atanh(Box::new(a)));
                }
                "x" => stack.push(Expression::Var),
                "PI" => stack.push(Expression::Num(std::f64::consts::PI)),
                "E" => stack.push(Expression::Num(std::f64::consts::E)),
                _ => stack.push(Expression::Num(token.parse().map_err(|_err| ())?)),
            }
        }
        stack.pop().ok_or(())
    }
}

fn main() {
    let f = Expression::from_str("1 2 PI * sqrt / E -1 2 / x 2 powf * powf *");
    match f {
        Ok(f) => println!("{}", f(2.0)),
        Err(_) => println!("Invalid expression!"),
    }
}
